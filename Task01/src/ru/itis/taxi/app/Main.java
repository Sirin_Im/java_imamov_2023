package ru.itis.taxi.app;

import ru.itis.taxi.dto.SignUpForm;
import ru.itis.taxi.mappers.Mappers;
import ru.itis.taxi.models.User;
import ru.itis.taxi.repositories.UsersRepository;
import ru.itis.taxi.repositories.UsersRepositoryFilesImpl;
import ru.itis.taxi.services.UsersService;
import ru.itis.taxi.services.UsersServiceImpl;

import java.util.UUID;

public class Main {

    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt");
        UsersService usersService = new UsersServiceImpl(usersRepository, Mappers::fromSignUpForm);
        usersService.signUp(new SignUpForm("Сирин", "Имамов",
                "sirin3002imamov@gmail.com", "qwerty001"));

        User user = new User("4ebccfb7-3515-4c15-913b-e81dd8e33394","Виталий","Комиссаров",
                "vk133081@gmail.com","qwerty008");
        usersRepository.update(user);

        System.out.println(usersRepository.findAll().toString());

        User user1 = new User("f68b4ea6-9ac2-4d01-bb18-3577855407ed","Сирин","Имамов",
                "sirin3002imamov@gmail.com","qwerty001");
        usersRepository.delete(user1);

        usersRepository.deleteById(UUID.fromString("565b7ead-3e7a-47fa-9a70-ce5f2148bbf5"));

        System.out.println(usersRepository.findById(UUID.fromString("e9f2f383-5951-4de9-8a49-d407dc87474c")).toString());
    }
}
