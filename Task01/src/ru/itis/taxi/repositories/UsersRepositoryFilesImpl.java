package ru.itis.taxi.repositories;

import ru.itis.taxi.models.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;

/**
 * 30.06.2022
 * 02. TaxiService
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class UsersRepositoryFilesImpl implements UsersRepository {

    private final String fileName;

    private static final Function<User, String> userToString = user ->
            user.getId()
                    + "|" + user.getFirstName()
                    + "|" + user.getLastName()
                    + "|" + user.getEmail()
                    + "|" + user.getPassword();

    public UsersRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();

        try(BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line = reader.readLine();
            while (line != null){
                String[] elems = line.split("\\|");
                users.add(new User(elems[0],elems[1],elems[2],elems[3],elems[4]));
                line = reader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        return users;
    }

    @Override
    public void save(User entity) {
        entity.setId(UUID.randomUUID());
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            String userAsString = userToString.apply(entity);
            bufferedWriter.write(userAsString);
            bufferedWriter.newLine();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User entity) {
        File source = new File(fileName);
        File output = new File("users2.txt");

        try(BufferedReader reader = new BufferedReader(new FileReader(source));
            BufferedWriter writer = new BufferedWriter(new FileWriter(output))) {
            String line = reader.readLine();
            ArrayList<String> lines = new ArrayList<>();

            while (line != null){
                if(line.startsWith(entity.getId().toString())){
                    line = userToString.apply(entity);
                }
                lines.add(line);
                line = reader.readLine();
            }

            for (String s : lines) {
                writer.write(s);
                writer.write('\n');
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        source.delete();
        output.renameTo(source);
    }

    @Override
    public void delete(User entity) {
        File source = new File(fileName);
        File output = new File("users2.txt");

        try(BufferedReader reader = new BufferedReader(new FileReader(source));
            BufferedWriter writer = new BufferedWriter(new FileWriter(output))) {
            String line = reader.readLine();
            ArrayList<String> lines = new ArrayList<>();

            while (line != null){
                if(!line.startsWith(entity.getId().toString())){
                    lines.add(line);
                }
                line = reader.readLine();
            }

            for (String s : lines) {
                writer.write(s);
                writer.write('\n');
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        source.delete();
        output.renameTo(source);
    }

    @Override
    public void deleteById(UUID id) {
        File source = new File(fileName);
        File output = new File("users2.txt");

        try(BufferedReader reader = new BufferedReader(new FileReader(source));
        BufferedWriter writer = new BufferedWriter(new FileWriter(output))) {
            String line = reader.readLine();
            ArrayList<String> lines = new ArrayList<>();

            while (line != null){
                if(!line.startsWith(id.toString())){
                    lines.add(line);
                }
                line = reader.readLine();
            }

            for (String s : lines) {
                writer.write(s);
                writer.write('\n');
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        source.delete();
        output.renameTo(source);
    }

    @Override
    public User findById(UUID id) {
        try(BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line = reader.readLine();

            while (line != null){
                String[] elems = line.split("\\|");
                if (elems[0].equals(id.toString())){
                    return new User(elems[0],elems[1],elems[2],elems[3],elems[4]);
                }
                line = reader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        return null;
    }
}
