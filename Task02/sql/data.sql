insert into client(first_name,phone_number)
values ('Сирин','89047777777'),
       ('Виталий','89627777777'),
       ('Влад','89997777777');

insert into driver(first_name,phone_number,driving_experience)
values ('Айрат','89048888888',12),
       ('Озазилло','89628888888',23),
       ('Александр','89998888888',9);

insert into car(brand, model, color, number)
values ('Lada','XRAY','black','x806xx'),
       ('Nissan','Almera','white','y411yy'),
       ('Skoda','Rapid','red','z999zz');

insert into ordering(client_name, driver_name, order_cost)
values ('Сирин','Айрат',125),
       ('Виталий','Айрат',230),
       ('Влад','Озазилло',965);

update client set phone_number = '88087124447' where id = 1;
update driver set driving_experience = 10 where id = 3;
