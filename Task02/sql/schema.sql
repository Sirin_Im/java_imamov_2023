drop table if exists client;
drop table if exists driver;
drop table if exists car;
drop table if exists ordering;

create table client(
    id bigserial primary key,
    first_name char(20),
    phone_number char(11)
);

create table driver(
    id bigserial primary key,
    first_name char(20),
    phone_number char(11),
    driving_experience bigint check (driving_experience > 5 and driving_experience < 40)
);

create table car(
    id bigserial primary key,
    brand char(20),
    model char(11),
    color char(20),
    number char(6) not null default ''
);

create table ordering(
     id bigserial primary key,
     client_name char(20),
     driver_name char(20),
     order_cost bigint
);

alter table ordering add client_id bigint;
alter table ordering add driver_id bigint;
alter table ordering add foreign key(client_id) references client(id);
alter table ordering add foreign key(driver_id) references driver(id);
