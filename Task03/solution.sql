-- 1
select model, speed, hd from pc where price < 500;

-- 2
select distinct(maker) from product where type = 'Printer';

-- 3
select model, ram, screen from laptop where price > 1000;

-- 4
select * from printer where color = 'y';

-- 5
select model, speed, hd from pc where (cd = '12x' or cd = '24x') and price < 600;

-- 6
select distinct(maker), speed  from product inner join laptop on product.model = laptop.model where hd >= 10;

-- 7
select laptop.model , laptop.price  from laptop inner join product on laptop.model = product.model
    where product.maker= 'B' union select pc.model , pc.price from pc inner join product on pc.model = product.model
    where product.maker= 'B' union select printer.model , printer.price from printer inner join product on printer.model = product.model
    where product.maker= 'B';

-- 8
select maker from product where type = 'PC' and maker not in (select maker from product where type = 'Laptop') group by maker;

-- 9
Select maker  from pc inner join product on pc.model = product.model where speed >= 450 group by maker;

-- 10
select model, price from printer where price in (select max(price) from printer);

-- 11
select avg(speed) from pc;

-- 12
select avg(speed) from laptop where price > 1000;

-- 13
select avg(speed) from pc inner join product on pc.model = product.model where maker = 'A';

-- 15
select hd  from pc group by hd having count(model) > 1;

-- 16
select distinct B.model AS model, A.model AS model, A.speed, A.ram from PC AS A, PC B
    where A.speed = B.speed AND A.ram = B.ram and A.model < B.model;

-- 17
select distinct type,laptop.model,speed from laptop inner join product on laptop.model= product.model
    where speed < (select MIN(speed) from pc);

--18
select distinct maker, price from printer inner join product on printer.model = product.model
    where color = 'y' and price = (select min(price) from printer where color = 'y');

-- 19
select maker ,avg(screen) from laptop inner join product on laptop.model =  product.model group by maker;

-- 20
select maker , count(model) from product where type = 'pc' group by maker having count(model) >= 3;

-- 21
select maker , max(price) from pc inner join product on pc.model= product.model group by maker;

-- 22
select speed , avg(price) from pc  where speed > 600 group by speed;

-- 23
select distinct maker  from pc inner join product on pc.model = product.model
    where pc.speed >= 750 and maker in (
        select  maker from laptop inner join product on laptop.model = product.model where laptop.speed >= 750);

-- 24
select model from(
    select distinct model, price from laptop where laptop.price = (select max(price) from laptop)
    union
    select distinct model, price from pc where pc.price = (select max(price) from pc)
    union
    select distinct model, price from printer where printer.price = (select max(price) from printer)
    ) as t
where t.price = (select max(price) from (
    select distinct price from laptop where laptop.price = (select max(price) from laptop)
    union
    select distinct price from pc where pc.price = (select max(price) from pc)
    union
    select distinct price from printer where printer.price = (select max(price) from printer))

--27
select maker,avg(hd)  from product inner join pc on product.model=pc.model
    where maker in(select maker  from product  where type='printer')  group by maker;

-- 29
select point, date, SUM(inc), sum(out) from (select point, date, inc, null as out from Income_o
    union select point, date, null as inc, Outcome_o.out from Outcome_o)as t group by t.point, t.date;

-- 30
select point, date, SUM(sum_out), SUM(sum_inc)
from (select point, date, SUM(inc) as sum_inc, null as sum_out from income group by point, date
union
select point, date, null as sum_inc, SUM(out) as sum_out from outcome group by point, date ) as t
group by point, date order by point;

-- 31
Select class , country from classes where bore >= 16;

-- 33
Select ship from outcomes,battles where result= 'sunk' and battle = 'North Atlantic' group by ship;

-- 34
select name  from classes inner join ships on ships.class = classes.class
    where launched >= 1922 and displacement>35000 and type='bb';

-- 35
select model, type from product where model not like '%[^0-9]%' or model not like '%[^a-z]%';

-- 36
select name  from ships  where class = name
union
select ship from classes,outcomes  where classes.class = outcomes.ship;

-- 38
select distinct country  from classes  where type='bb'
    intersect select distinct country  from classes  where type='bc';

-- 42
Select ship,battle from outcomes where result ='sunk';

-- 44
select name from ships where name like 'R%'
union select name from battles where name like 'R%'
union select ship from outcomes where ship like 'R%';

-- 45
select name from ships where name like '% % %'
union
select ship from outcomes where ship like '% % %';

-- 48
select class as n from ships where name in(select ship from outcomes where result = 'sunk')
union
select ship as n from outcomes where ship not in(Select name from ships)
    and ship in(Select class from classes) and result = 'sunk';

-- 49
select name from ships where class in( Select class from classes where bore = 16)
union
select ship from outcomes where ship in( Select class from classes where bore = 16);

-- 50
select distinct battle from classes inner join ships on ships.class = classes.class
                                    inner join outcomes on classes.class = outcomes.ship or ships.name = outcomes.ship
                                        where classes.class = 'Kongo';

-- 52
select distinct name from ships  inner join classes cl on ships.class = cl.class
where (numGuns >= 9 or numguns is NULL) and (bore < 19 or bore is NULL)
                                        and (displacement <= 65000 or displacement is NULL)
                                        and type = 'bb' and country = 'japan';


-- 55
select C.class, min(launched) from ships as S right join classes as C on s.class = c.class group by C.class;

