package ru.itis;

import ru.itis.jdbc.SimpleDataSource;
import ru.itis.models.Student;
import ru.itis.repositories.StudentsRepository;
import ru.itis.repositories.StudentsRepositoryJdbcTemplateImpl;
import ru.itis.services.StudentsService;
import ru.itis.services.StudentsServiceImpl;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

public class Main {


    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(Main.class.getResourceAsStream("/db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcTemplateImpl(dataSource);

        Student student = Student.builder()
                .firstName("Сирин")
                .lastName("Имамов")
                .email("email")
                .password("qwert007")
                .build();
        System.out.println(student);
        studentsRepository.save(student);
        System.out.println(student);

        Student student1 = Student.builder()
                .id(3L)
                .firstName("Виталий")
                .lastName("Комиссаров")
                .age(20)
                .email("mail")
                .password("qwert007")
                .build();
        studentsRepository.update(student1);

        studentsRepository.delete(3L);

        System.out.println(studentsRepository.findAllByAgeGreaterThanOrderByIdDesc(10));
    }
}
